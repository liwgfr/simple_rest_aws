package com.binocla.resources;

import com.binocla.models.Fruit;
import com.binocla.services.FruitAsyncService;
import io.smallrye.mutiny.Uni;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/fruits")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.TEXT_HTML)
public class FruitAsyncResource {

    @Inject
    FruitAsyncService service;

    @GET
    public Uni<List<Fruit>> getAll() {
        return service.findAll();
    }

    @GET
    @Path("{name}")
    public Uni<Fruit> getSingle(@PathParam("name") String name) {
        return service.get(name);
    }

    @POST
    public Uni<List<Fruit>> add(@QueryParam("name") String name, @QueryParam("description") String description) {
        Fruit fruit = new Fruit();
        fruit.setName(name);
        fruit.setDescription(description);
        return service.add(fruit)
                .onItem().ignore().andSwitchTo(this::getAll);
    }

    @DELETE
    @Path("{name}")
    public Uni<String> delete(@PathParam("name") String name) {
        return service.remove(name);
    }
}
